
export interface RootObject {
    shipmentHistory: ShipmentHistory[];
}

export interface ShipmentHistory {
    eventDateTime: string;
    shipmentIsDelayed: boolean;
    eventPosition: EventPosition;
}

export interface EventPosition {
    status: string;
    comments: string;
    city?: string;
    state?: string;
    country?: string;
}


const shipmentHistory: ShipmentHistory[] = [{
    "eventDateTime": "2019-10-21T15:07:00",
    "shipmentIsDelayed": false,
    "eventPosition":{
        "status": "DELIVERED",
        "comments": " Signed by Joe",
        "city": "FREDERICK",
        "state": "MD",
        "country": "USA"
    }
},
{
    "eventDateTime": "2019-10-21T15:06:00",
    "shipmentIsDelayed": false,
    "eventPosition": {
        "status": "ARRIVED AT YOUR PLACE",
        "comments": "null",
        "city": "FREDERICK",
        "state": "MD",
        "country": "USA"
    }
},
{
    "eventDateTime": "2019-10-21T08:00:00",
    "shipmentIsDelayed": false,
    "eventPosition": {
        "status": "OUT FOR DELIVERY",
        "comments": "null",
        "city": "BELTSVILLE",
        "state": "MD",
        "country": "USA"
    }
},
{
    "eventDateTime": "2019-10-21T05:50:00",
    "shipmentIsDelayed": false,
    "eventPosition": {
        "status": "DELIVERY SCHEDULED",
        "comments": "null",
        "city": "BELTSVILLE",
        "state": "MD",
        "country": "USA"
    }
},
{
    "eventDateTime": "2019-10-09T14:00:00",
    "shipmentIsDelayed": false,
    "eventPosition": {
        "status": "ARRIVED",
        "comments": "Arrived At Stop",
        "city": "BELTSVILLE",
        "state": "MD",
        "country": "USA"
    }
},
{
    "eventDateTime": "2019-10-09T13:48:24.374",
    "shipmentIsDelayed": false,
    "eventPosition": {
        "status": "PICK UP SCHEDULE",
        "comments": "null",
        "city": "NEW YORK",
        "state": "NY",
        "country": "USA"
    }
},
{
    "eventDateTime": "2019-10-06T11:35:24.374",
    "shipmentIsDelayed": true,
    "eventPosition": {
        "status": "PICK UP DELAYED",
        "comments": "Road Closed -Weather",
        "city": "NEW YORK",
        "state": "NY",
        "country": "USA"
    }
},
{
    "eventDateTime": "2019-10-04T21:03:30.816",
    "shipmentIsDelayed": false,
    "eventPosition": {
        "comments": "null",
        "status": "ARRIVED"
    }
}
]

export const data: RootObject={
    shipmentHistory:shipmentHistory
};