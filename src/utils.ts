import { History } from "./classes/History";


// Status enumerations
export enum Status {
    "DELIVERED"='<i class="far fa-check-circle mb-6 F check"></i>',
    "ARRIVED AT LOCAL FACILITY" = '<i class="fal fa-map-marker-alt F locateIcon"></i>',

}



// Months enumerations
export enum Months {
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
}

// nth function showing st rd nd in the date like 1st 2nd 3rd
export const NTH = function (day: number) {
    if (day > 3 && day < 21) return 'th';
    switch (day % 10) {
        case 1: return "st";
        case 2: return "nd";
        case 3: return "rd";
        default: return "th";
    }
}


// Generate list of unique dates and theirs index
export const genListDateToShow = function (item: History[]) {
    let tab:string[]= []
    let show = []
   

    for(let index=0;index<item.length;index++){
        if(!tab.includes(item[index].getDate())){
            let d = item[index]?.getDate()
            show.push(index)
            tab.push(d)
        }
        
        
    }

    return show
}

export const getDate =(eventDateTime:string)=>{
    let itemDate = new Date(eventDateTime)
    let year = itemDate.getFullYear()
    let month = itemDate.getMonth()
    let day = itemDate.getUTCDate()
    return `${year}-${month}-${day}`
}