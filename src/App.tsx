import React from 'react';

/*
*  MY OWN DATASETS BASED ON BENNU IMAGES
*  You can uncomment to try it but don't forget to comment the BENNU DATASET
*/

import * as data from './dataset.json'

// BENNU DATASET
// import { data } from "./mock_data";

// UTILS functions
import { Months, NTH, getDate } from './utils'
// Styles
import './App.css';

const length = data.shipmentHistory.length
function App() {

  return (
    <>
      <div className="container">
        <table>
          <caption className="title">
            <span className="C">Shipment History</span>
          </caption>
          {/* This is where all the magic happens */}
          <tbody id="app">
            {data.shipmentHistory.map((item, index) => {
              return <AddTotheDom item={item} key={index} index={index} />
            })}
          </tbody>
        </table>
      </div>
    </>
  );
}

const AddTotheDom = ({ ...props }) => {

  const { item, index } = props
  const { eventDateTime, shipmentIsDelayed, eventPosition } = item
  let date = new Date(eventDateTime)

  const Dot = () => {
    if (shipmentIsDelayed) {
      return (<div className="dotYellow"></div>)
    } else if(eventPosition.status==="ARRIVED AT LOCAL FACILITY") {
      return <i className="fal fa-map-marker-alt F locateIcon"></i>
    } else if(eventPosition.status==="DELIVERED"){
      return <i className="fal fa-map-marker-alt F locateIcon"></i>
    }else{
      return (<div className="dotBlue"></div>)
    }
  }

  const Path = () => {
    if ((index < length - 1) && data.shipmentHistory[index + 1].shipmentIsDelayed) {
      return (<div className='pathYellow'></div>)
    }
    if (shipmentIsDelayed) {
      return (<div className="pathYellow"></div>)
    }
    else if(index===length-1){
      return <div></div>
    }else{
      return (<div className="pathBlue"></div>)
    }
  }

  const DateField = ({...props})=>{
    const {itemDate} = props
    if(index>=1&&index<length-1&&getDate(data.shipmentHistory[index].eventDateTime)===getDate(data.shipmentHistory[index+1].eventDateTime)){
      return(
        <div></div>
      )
    }else{
      return(<div>{Months[itemDate.getMonth()] + " " + itemDate.getUTCDate() + NTH(itemDate.getUTCDate())}</div>)
    }
    
  }

  return (
    <tr>
      <td>
        <div className="timeInfo">
          <DateField itemDate={date} index={index} />
          <div className="A">{date.toLocaleTimeString("en-US", { hour: "numeric", minute: "numeric" }).toLowerCase()}</div>
        </div>
      </td>
      <td className="pathAssets">
        <Dot />
        <Path />
      </td>
      <td className="statusBox">
        <div className="statusInfo">
          <div className="status B"><span className="statusTitle">{eventPosition.status.toLowerCase()}</span></div>
          <span className="A">{eventPosition.comments === 'null' ? '' : eventPosition.comments}</span>
        </div>
        <div className="locationInfo A">
          <div className="A town">{eventPosition?.city?.toLowerCase() || ""},</div>
          <div className="A">&nbsp;{eventPosition?.state || ""}</div>
        </div>
      </td>
    </tr>
  )
}

export default App;
