import { EventPosition } from "../mock_data";

export class History{
    public eventDateTime : string;
    public shipmentIsDelayed: boolean;
    public eventPosition:EventPosition = {status:"",comments:""}

    constructor(eventDateTime:string,shipmentIsDelayed: boolean,eventPosition:EventPosition){
        this.eventDateTime=eventDateTime;
        this.shipmentIsDelayed = shipmentIsDelayed
        this.eventPosition = eventPosition
    }
    public getDate():string{
        let date = new Date(this.eventDateTime)
        let year = date.getFullYear()
        let month = date.getMonth()
        let day = date.getUTCDate()

        return `${year}-${month}-${day}`
    }
}